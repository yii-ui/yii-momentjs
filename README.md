[![Yii UI](https://avatars1.githubusercontent.com/u/22790740?s=60)](https://www.yii-ui.com/) Yii2 Asset for Moment.js
================================================

[![Latest Stable Version](https://poser.pugx.org/yii-ui/yii-momentjs/version)](https://packagist.org/packages/yii-ui/yii-momentjs)
[![Total Downloads](https://poser.pugx.org/yii-ui/yii-momentjs/downloads)](https://packagist.org/packages/yii-ui/yii-momentjs)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)
[![License](https://poser.pugx.org/yii-ui/yii-momentjs/license)](https://packagist.org/packages/yii-ui/yii-momentjs)


This is an [Yii framework 2.0](http://www.yiiframework.com) asset for [Moment.js](https://momentjs.com/).

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require yii-ui/yii-momentjs
```
or add
```
"yii-ui/yii-momentjs": "dev-master"
```
to the require section of your `composer.json` file.

Usage
-----

in your layout file (ex. views/layouts/main.php):
```php
\yiiui\momentjs\MomentJsAsset::register($this);
```

or as dependency in your app asset bundle:
```php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';

    public $js = [
        'js/main.js',
    ];

    public $css = [
        'css/main.scss',
    ];

    public $depends = [
        'yiiui\momentjs\MomentJsAsset'
    ];
}
```

More [Examples](https://www.yii-ui.com/yii-momentjs) will be added soon at https://www.yii-ui.com/yii-momentjs.
For full list of icons see Material Design Icons [Documentation](https://materialdesignicons.com/).

Documentation
------------

[Documentation](https://www.yii-ui.com/yii-momentjs) will be added soon at https://www.yii-ui.com/yii-momentjs.

License
-------

**yii-momentjs** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
