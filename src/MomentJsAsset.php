<?php
namespace yiiui\momentjs;

use yii\web\AssetBundle;

class MomentJsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/moment/moment/min';

    public $js = [
        'moment-with-locales.min.js'
    ];
}
